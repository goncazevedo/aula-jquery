$(document).ready(function () {
    $("#example").on("click","button.switch",function() {
        $.ajax("https://jquery-19-1.herokuapp.com/resulat.html", {
            beforeSend: function () {
                $("#status").text("Carregando!")
            }
        })
        .done(function(response){
            $("#result").html(response);
            
        })
        .fail(function(request, errorType, errorMessage){
            alert(errorMessage);
            console.log(errorType);
        })
        .always(function () {
            $("#status").text("Completo");
        });
    });
})