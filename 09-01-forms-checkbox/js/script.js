$(document).ready(function () {

    $("#hasPets").on("change", function () {
        if($(this).is(":checked")){
            $("#pets-row").show()
        }
        else{
            $("#pets-row").hide()
        }
        
    })

    $("#pet").on("change", function () {
        if($(this).val() == "Dog") {
            $("#pet-feedback").text("CACHORRO SAO LINDO E MARAVILHOSO");
        }
        else if ($(this).val() == "Cat"){
            $("#pet-feedback").text("GATO SAO LINDO E MARAVILHOSO");
        }
        else if ($(this).val() == "Human"){
            $("#pet-feedback").text("HUMANO NOJENTO GORE");
        }
        else {
            $("#pet-feedback").text("");
        }
        
        
    })

    $("#hasPets").trigger("change");
});